package com.stock_test.mvc.dao;

import java.util.List;

public interface IGenericDao<E> {
	
	public E save(E entity);
	
	public E update(E entity);
	
	public List<E> selectAll();
	
	public List<E> selectAll(String sortField, String sort);
	
	public E getById(Long id);
	
	public void remove(Long id);
	
	public E findOne(String paramNam, Object[] paramValues);
	
	public E findOne(String[] paramNam, Object[] paramValues);
	
	public int findCountBy(String paramNam, Object[] paramValues);
	

}
