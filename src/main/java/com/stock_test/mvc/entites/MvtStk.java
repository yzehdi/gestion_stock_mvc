package com.stock_test.mvc.entites;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class MvtStk implements Serializable{
	
	public static final int ENTREE = 1;
	public static final int SORTIE = 2;
	
	
	@Id
	@GeneratedValue
	private Long idMvtStk;
	
	private BigDecimal quantite;
	
	private int typeMVT;
	
	@ManyToOne
	@JoinColumn(name = "idArticle")
	private Article article;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateMvt;

	public Long getIdMvtStk() {
		return idMvtStk;
	}

	public void setIdMvtStk(Long id) {
		this.idMvtStk = id;
	}

	public BigDecimal getQuantite() {
		return quantite;
	}

	public void setQuantite(BigDecimal quantite) {
		this.quantite = quantite;
	}

	public int getTypeMVT() {
		return typeMVT;
	}

	public void setTypeMVT(int typeMVT) {
		this.typeMVT = typeMVT;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public Date getDateMvt() {
		return dateMvt;
	}

	public void setDateMvt(Date dateMvt) {
		this.dateMvt = dateMvt;
	}
	
	

}
